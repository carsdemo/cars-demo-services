const env = require('./app/util/config');
const app = require('./app/routes');
app.listen(env.PORT);