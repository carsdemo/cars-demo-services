const Sequelize = require('sequelize');
const sequelize = require('../util/database');

const Car = sequelize.define('car', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    description: Sequelize.STRING,
    model: Sequelize.STRING,
    make: Sequelize.STRING,
    km: Sequelize.INTEGER,
    estimatedDate: Sequelize.DATE,
    image: Sequelize.STRING,
    person: Sequelize.STRING
});

module.exports = Car;