const express = require('express');
const app = express();
const cors = require('cors');
const env = require('../util/config');

// Routes
const carRoutes = require('./cars.routes');

app.use(cors());
app.use(express.json());

app.use(`${env.API}car`, carRoutes);

module.exports = app;