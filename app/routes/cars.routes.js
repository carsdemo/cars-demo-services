const express = require('express');
const router = express.Router();
const carsController = require('../controllers/cars.controller');

/**
 * Get all cars route
 */
router.get('/', carsController.getAllCars);
/**
 * Add maintenance date and person to car
 */
router.put('/', carsController.updateCar);

module.exports = router;