const Car = require('../models/car.model');

/**
 * Get all cars
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.getAllCars = (req, res, next) => {
    Car.findAll().then((c) => {
        return res.json(c);
    }).catch((e) => {
        return res.status(200).json([]);
    })
}

/**
 * UpdateCar
 * @param {*} req {id: number; person: string; date: date}
 * @param {*} res 
 * @param {*} next 
 */
exports.updateCar = (req, res, next) => {
    const id = req.body.id;
    Car.findByPk(id).then((c) => {
        c.person = req.body.person;
        c.estimatedDate = req.body.date;
        c.save();
        return c;
    }).then((c) => {
        return res.json(c);
    }).catch((e) => {
        return res.status(500);
    })
}
